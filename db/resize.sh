#! /bin/bash

# check params count
if [ "$#" -ne 2 ]; then
  echo "error: 2 parameters needed"
  echo "usage: sh resize.sh FOLDER MAXSIZE"
  echo "example (from project's root): sh db/resize.sh db/pictures 720"
  exit 1
fi

FOLDER=$1
MAXSIZE=$2

find $FOLDER -name "*.jpg" | while read file
do
  # check if image needs to be resize
  width=$(identify -format '%w' $file)
  height=$(identify -format '%h' $file)
  if [ $width -gt $MAXSIZE ] || [ $height -gt $MAXSIZE ]
  then
    before=$(du -k $file | cut -f1)
    mogrify -resize $(echo "${MAXSIZE}x${MAXSIZE}>") $file
    echo "$file: ${before}KB (${width}x${height}) --> $(du -k $file | cut -f1)KB ($(identify -format '%w' $file)x$(identify -format '%h' $file))"
  else
    echo "$file: no resize (${width}x${height})"
  fi
done
