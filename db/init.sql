INSERT INTO place (id, name, hint, difficulty, geom)
VALUES

    (1, 'Tour Eiffel', 'Voulez-vous coucher avec vous, ce soir ?', 1, ST_SetSRID(ST_MakePoint(2.294499, 48.858260),4326)),
    (2, 'Taj Mahal', NULL, 1, ST_SetSRID(ST_MakePoint(78.042096, 27.175012),4326)),
    (3, 'Gizeh Pyramids', 'Oh my beautiful son!', 1, ST_SetSRID(ST_MakePoint(31.13290, 29.97711),4326)),
    (4, 'Kremlin', 'Da kamarad', 1, ST_SetSRID(ST_MakePoint(37.617664, 55.752121),4326)),
    (5, 'Statue of Liberty', 'He got it', 1, ST_SetSRID(ST_MakePoint(-74.044502, 40.689247),4326)),
    (6, 'Mount Rushmore', 'America, f*** yeah !', 1, ST_SetSRID(ST_MakePoint(-103.4590070,43.8790446),4326)),
    (7, 'Tower of Pisa', 'Insta time !', 1, ST_SetSRID(ST_MakePoint(10.3966089,43.7230283),4326)),
    (8, 'Sydney Opera', 'Tell him, silvery moon, that I am embracing him', 1, ST_SetSRID(ST_MakePoint(151.2152037,-33.8567250),4326)),
    (9, 'Burj Khalifa', 'How much ?', 1, ST_SetSRID(ST_MakePoint(55.274288, 25.197525),4326)),
    (10, 'Mont Saint-Michel', 'I''m N !', 1, ST_SetSRID(ST_MakePoint(-1.511400, 48.636002),4326)),
    (11, 'The Acropolis', 'γεια', 1, ST_SetSRID(ST_MakePoint(23.7257554,37.9715572),4326)),
    (12, 'Gibraltar', 'Go fast', 1, ST_SetSRID(ST_MakePoint(-5.346250,36.109355),4326)),
    (13, 'Golden Gate Bridge', 'XXX', 1, ST_SetSRID(ST_MakePoint(-122.478230,37.820027),4326)),
    (14, 'The Western Wall', 'Shalom', 1, ST_SetSRID(ST_MakePoint(35.234508, 31.776848),4326)),
    (15, 'Buckingham Palace', 'Dog save the queen', 1, ST_SetSRID(ST_MakePoint(-0.1418841,51.5013762),4326)),
    (16, 'La Sagrada Familia', 'Dali dali di, i dali dali da', 1, ST_SetSRID(ST_MakePoint(2.1743067,41.4036957),4326)),
    (17, 'Christ the Redeemer', 'Corcovado', 1, ST_SetSRID(ST_MakePoint(-43.2103812,-22.9522970),4326)),
    (18, 'Blue Mosque', 'Foot locker', 1, ST_SetSRID(ST_MakePoint(28.9768202,41.0054518),4326)),
    (19, 'The Colosseum', 'Spartacus', 1, ST_SetSRID(ST_MakePoint(12.492373, 41.890251),4326)),
    (20, 'Hollywood', 'Chewing-gum a capella', 1, ST_SetSRID(ST_MakePoint(-118.3215456,34.1341205),4326)),
    (21, 'Check Point Charlie', 'Das Leben der Anderen', 1, ST_SetSRID(ST_MakePoint(13.39039098,52.50745733),4326)),
    (22, 'Mount Fuji', 'Rising sun', 1, ST_SetSRID(ST_MakePoint(138.730167,35.363149),4326)),
    (23, 'Wall Street', 'L''empereur l''investisseur (cryptopu*e)', 1, ST_SetSRID(ST_MakePoint(-74.008827, 40.706005),4326)),
    (24, 'Forbidden City', '*** ** **** **** * ?', 1, ST_SetSRID(ST_MakePoint(116.3885829,39.9098075),4326)),
    (25, 'Kaaba', NULL, 1, ST_SetSRID(ST_MakePoint(39.8261839,21.4225474),4326)),

    (26, 'Machu Picchu', NULL, 2, ST_SetSRID(ST_MakePoint(-72.5449653,-13.1631068),4326)),
    (27, 'Angkor Wat', NULL, 2, ST_SetSRID(ST_MakePoint(103.8670137,13.4125836),4326)),
    (28, 'Petra', 'Patatrah !', 2, ST_SetSRID(ST_MakePoint(35.444832, 30.328960),4326)),
    (29, 'Ha Long Bay', NULL, 2, ST_SetSRID(ST_MakePoint(107.13288,20.89273),4326)),
    (30, 'Stonehenge', NULL, 2, ST_SetSRID(ST_MakePoint(-1.826215, 51.178882),4326)),
    (31, 'Grand Palace', NULL, 2, ST_SetSRID(ST_MakePoint(100.48833138, 13.742830362),4326)),
    (32, 'Easter Island', NULL, 2, ST_SetSRID(ST_MakePoint(-109.360481, -27.104671),4326)),
    (33, 'Cape of Good Hope', NULL, 2, ST_SetSRID(ST_MakePoint(18.474037,-34.356773),4326)),
    (34, 'FernsehTurm', NULL, 2, ST_SetSRID(ST_MakePoint(13.4094116,52.5208422),4326)),

    (50, 'Beg Lann', 'Coco jumbos', 3, ST_SetSRID(ST_MakePoint(-2.740030, 47.500527),4326)),
    (51, 'Gaschney', 'Merci ! Serrrvice !', 3, ST_SetSRID(ST_MakePoint(7.0450465,48.0337719),4326)),
    (52, 'Buzludzha', 'Un pueblo unido', 3, ST_SetSRID(ST_MakePoint(25.3940554,42.7357517),4326)),
    (53, 'Ryugyong', NULL, 3, ST_SetSRID(ST_MakePoint(125.7309755,39.0364973),4326)),
    (54, 'Princess Elisabeth Station', 'x+2y²=-3i', 3, ST_SetSRID(ST_MakePoint(23.347079, -71.949944),4326))

ON CONFLICT (id) DO UPDATE
    SET name = excluded.name,
        hint = excluded.hint,
        difficulty = excluded.difficulty,
        geom = excluded.geom;

INSERT INTO picture (id, place_id)
VALUES

    (1, 1), (2, 1),
    (3, 2), (4, 2),
    (5, 3), (6, 3),
    (7, 4), (8, 4),
    (9, 5), (10, 5),
    (11, 6), (12, 6),
    (13, 7), (14, 7),
    (15, 8), (16, 8),
    (19, 10), (20, 10),
    (21, 11), (22, 11),
    (23, 12), (24, 12),
    (25, 13), (26, 13),
    (27, 14), (28, 14),
    (29, 15), (30, 15)

ON CONFLICT (id) DO UPDATE
    SET place_id = excluded.place_id;

BEGIN;
-- protect against concurrent inserts while you update the counter
LOCK TABLE place IN EXCLUSIVE MODE;
LOCK TABLE picture IN EXCLUSIVE MODE;
-- Update the sequences
SELECT SETVAL(PG_GET_SERIAL_SEQUENCE('place', 'id'), (SELECT MAX(d.m) + 1 FROM (SELECT 100 AS m UNION ALL SELECT MAX(id) AS m FROM place) AS d) + 1);
SELECT SETVAL(PG_GET_SERIAL_SEQUENCE('picture', 'id'), (SELECT MAX(d.m) + 1 FROM (SELECT 100 AS m UNION ALL SELECT MAX(id) AS m FROM picture) AS d) + 1);
COMMIT;
