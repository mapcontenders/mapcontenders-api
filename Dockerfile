FROM python:3.8-buster

MAINTAINER Guilhem Allaman "dev@guilhemallaman.net"

WORKDIR /app

ADD . /app
RUN mkdir -p pictures

RUN pip install poetry
RUN poetry install

CMD ["poetry", "run", "uwsgi", "app.ini"]
