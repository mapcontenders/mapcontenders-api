from flask import Flask
from flask_caching import Cache
from flask_cors import CORS

cache: Cache = Cache()
cors: CORS = CORS()


def register_extensions(app: Flask) -> None:
    cache.init_app(app)
    cors.init_app(app)
