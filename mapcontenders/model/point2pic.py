from datetime import datetime
from typing import List, Optional

from mapcontenders.model import GuessModel
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel


class Point2PicGuessSet(GuessModel):
    """Random guess set (sent to contender)"""

    # meta
    created: datetime = datetime.now()
    difficulty: Optional[int]
    # to guess
    place: PlaceModel
    pictures: List[PictureModel]


class Point2PicGuessTry(GuessModel):
    """Guess try (received from contender)"""

    # meta
    created: datetime
    submitted: datetime = datetime.now()
    difficulty: Optional[int]
    # to guess
    id_place: int
    id_pictures: List[int]
    # contender's guess
    guess_id_picture: int


class Point2PicGuessResult(GuessModel):
    """Guess oneshot result (sent to contender)"""

    # meta
    duration: int
    difficulty: Optional[int]
    # to guess
    place: PlaceModel
    pictures: List[PictureModel]
    # contender's guess
    guessed_picture: PictureModel
    # answer
    correct_picture: PictureModel
    # result
    success: bool
