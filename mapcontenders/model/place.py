from datetime import datetime

from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape
from sqlalchemy.sql.functions import now

from mapcontenders.const import SRID_CODE
from mapcontenders.database import db

MIN_DIFF = 1
MAX_DIFF = 3


class PlaceModel(db.Model):

    __tablename__ = "place"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    hint = db.Column(db.Text)
    difficulty = db.Column(db.Integer)
    geom = db.Column(Geometry("POINT", SRID_CODE), nullable=False)

    creation = db.Column(db.DateTime, nullable=False, default=datetime.now(), server_default=now())
    creator = db.Column(db.Text, default="anonymous", server_default="anonymous")

    # likes/dislikes counters
    likes = db.Column(db.Integer, nullable=False, server_default="0")
    dislikes = db.Column(db.Integer, nullable=False, server_default="0")

    # pictures FK
    pictures = db.relationship("PictureModel", back_populates="place", lazy=True)

    # scores FK
    scores_pic2point = db.relationship("Pic2PointScore", back_populates="place", lazy=True)
    scores_point2pic = db.relationship("Pic2PointScore", back_populates="place", lazy=True)

    @property
    def lon(self) -> float:
        return to_shape(self.geom).x

    @property
    def lat(self) -> float:
        return to_shape(self.geom).y

    @property
    def pictures_count(self) -> int:
        return len(self.pictures)

    def like(self) -> None:
        self.likes += 1

    def dislike(self) -> None:
        self.dislikes += 1
