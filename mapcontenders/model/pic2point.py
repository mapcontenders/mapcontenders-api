from datetime import datetime
from typing import List, Optional

from mapcontenders.model import GuessModel
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel


class Pic2PointGuessSet(GuessModel):
    """Random guess set (sent to contender)"""

    # meta
    created: datetime = datetime.now()
    difficulty: Optional[int]
    # to guess
    pictures: List[PictureModel]


class Pic2PointGuessTry(GuessModel):
    """Guess try (received from contender)"""

    # meta
    created: datetime
    submitted: datetime = datetime.now()
    difficulty: Optional[int]
    # to guess
    id_pictures: List[int]
    # contender's guess
    guess_lon: float
    guess_lat: float


class Pic2PointGuessResult(GuessModel):
    """Guess oneshot result (sent to contender)"""

    # meta
    duration: int
    difficulty: Optional[int]
    # to guess
    pictures: List[PictureModel]
    # contender's guess
    guessed_lon: float
    guessed_lat: float
    # answer
    place: PlaceModel
    # result
    distance: float
