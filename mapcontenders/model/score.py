import re
from datetime import datetime
from typing import Dict

from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape
from sqlalchemy.orm import declarative_mixin, declared_attr
from sqlalchemy.sql.functions import now

from mapcontenders.const import SRID_CODE
from mapcontenders.database import db
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel


class ScoreModel(db.Model):

    # Database table name
    __tablename__ = "score_abs"

    # attributes shared by all scores
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(50), nullable=False)
    difficulty = db.Column(db.Integer)
    submitted = db.Column(db.DateTime, nullable=False, default=datetime.now(), server_default=now())
    duration = db.Column(db.Integer, nullable=False)

    # Joined inheritance
    __mapper_args__ = {"polymorphic_identity": __tablename__, "polymorphic_on": type}


@declarative_mixin
class ScoreConcreteModel:

    # mypy fix
    __name__ = "score_concrete"

    # database table name
    @declared_attr
    def __tablename__(cls) -> str:
        # convert title-cased class name to snake-cased
        snaked = re.sub(r"(?<!^)(?=[A-Z])", "_", cls.__name__).lower()
        return f"score_{snaked}"

    # primary key id, reference to abstract score id
    @declared_attr
    def id(self) -> db.Column:
        return db.Column(db.Integer, db.ForeignKey("score_abs.id"), primary_key=True)

    # joined inheritance identity
    @declared_attr
    def __mapper_args__(cls) -> Dict:
        return {"polymorphic_identity": cls.__tablename__}


# many-to-many relation between pic2point score and displayed pictures (belonging to the same place)
pic2point_scores_pictures = db.Table(
    "pic2point_scores_pictures",
    db.Column("pic2point_score_id", db.Integer, db.ForeignKey("score_abs.id"), primary_key=True),
    db.Column("picture_id", db.Integer, db.ForeignKey("picture.id"), primary_key=True),
)


class Pic2PointScore(ScoreConcreteModel, ScoreModel, db.Model):

    # database table name
    @declared_attr
    def __tablename__(cls) -> str:
        return "score_pic2point"

    # FK place that player had to guess location for
    place_id = db.Column(db.Integer, db.ForeignKey("place.id"), nullable=False)
    place: PlaceModel = db.relationship("PlaceModel", back_populates="scores_pic2point", primaryjoin="Pic2PointScore.place_id == PlaceModel.id")

    # FK pictures sent to player so that he can try to locate the place described by those pictures (they all belonging to same place)
    pictures = db.relationship("PictureModel", secondary=pic2point_scores_pictures, lazy=True)

    # distance between player's guess and real place's location
    distance = db.Column(db.Float, nullable=False)

    # point (GPS lon/lat) that player sent as a try
    guessed_geom = db.Column(Geometry("POINT", SRID_CODE), nullable=False)

    def __repr__(self) -> str:
        return f"[Pic2Point score #{self.id}: {self.distance}km to '{self.place.name}']"

    @property
    def lon(self) -> float:
        return to_shape(self.guessed_geom).x

    @property
    def lat(self) -> float:
        return to_shape(self.guessed_geom).y


# many-to-many relation between point2pic score and proposed pictures (belonging to different places)
point2pic_scores_pictures = db.Table(
    "point2pic_scores_pictures",
    db.Column("point2pic_score_id", db.Integer, db.ForeignKey("score_abs.id"), primary_key=True),
    db.Column("picture_id", db.Integer, db.ForeignKey("picture.id"), primary_key=True),
)


class Point2PicScore(ScoreConcreteModel, ScoreModel, db.Model):

    # database table name
    @declared_attr
    def __tablename__(cls) -> str:
        return "score_point2pic"

    # FK place sent to player whose location was displayed in his map
    place_id = db.Column(db.Integer, db.ForeignKey("place.id"), nullable=False)
    place: PlaceModel = db.relationship("PlaceModel", back_populates="scores_point2pic", primaryjoin="Point2PicScore.place_id == PlaceModel.id")

    # FK pictures sent to player, player has to select one of them after looking at the given place location on the map
    pictures = db.relationship("PictureModel", secondary=point2pic_scores_pictures, lazy=True)

    # if correct picture was guessed among all the proposed ones, so if players guessed the correct place (which guessed picture belongs to)
    success = db.Column(db.Boolean, nullable=False)

    # FK picture guessed by player
    guessed_picture_id = db.Column(db.Integer, db.ForeignKey("picture.id"), nullable=False)
    guessed_picture: PictureModel = db.relationship("PictureModel", primaryjoin="Point2PicScore.guessed_picture_id == PictureModel.id")

    def __repr__(self) -> str:
        success_str = "good" if self.success else "false"
        return f"[Point2Pic score #{self.id}: {success_str} picture guessed for {self.place.name}]"
