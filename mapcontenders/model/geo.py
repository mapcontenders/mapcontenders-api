from geoalchemy2 import Geometry
from geoalchemy2.functions import GenericFunction
from sqlalchemy.dialects.postgresql import BYTEA


class ST_Transform(GenericFunction):
    name = "ST_Transform"
    type = Geometry


class ST_AsMVTGeom(GenericFunction):
    name = "ST_AsMVTGeom"
    type = Geometry


class ST_AsMVT(GenericFunction):
    type = BYTEA
