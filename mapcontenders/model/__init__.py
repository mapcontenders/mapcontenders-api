# declare db models here so that when this module is imported, all models are also imported
from pydantic import BaseModel

from .picture import PictureModel
from .place import PlaceModel
from .score import Pic2PointScore, Point2PicScore, ScoreModel


class GuessModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True
