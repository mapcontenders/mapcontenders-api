import os
from datetime import datetime
from os.path import dirname

from flask import current_app
from PIL import Image, ImageOps
from sqlalchemy.sql.functions import now

from mapcontenders.database import db
from mapcontenders.model.place import PlaceModel


class PictureModel(db.Model):

    __tablename__ = "picture"

    id = db.Column(db.Integer, primary_key=True)
    creation = db.Column(db.DateTime, nullable=False, default=datetime.now(), server_default=now())

    # FK
    place_id = db.Column(db.Integer, db.ForeignKey("place.id"), nullable=False)
    place: PlaceModel = db.relationship("PlaceModel", back_populates="pictures", primaryjoin="PictureModel.place_id == PlaceModel.id")

    @property
    def filepath(self) -> str:
        """
        Computes file path for picture
        -> PICTURES_FOLDER/<place_id>/<picture_id>.jpg
        """
        return os.path.join(current_app.config["PICTURES_FOLDER"], f"{self.place_id}", f"{self.id}.jpg")

    def save_on_filesystem(self, image: Image) -> str:
        """
        Saves the uploaded file on FileSystem with correct path, after formatting image with Pillow.
        Returns the path where image file is stored
        """
        # resize if necessary
        width, height = image.size
        max_dim = current_app.config["MAX_PICTURE_DIMENSION"]
        if width > max_dim or height > max_dim:
            image.thumbnail((max_dim, max_dim), Image.ANTIALIAS)
        # apply exif (rotation) and save on FileSystem
        with ImageOps.exif_transpose(image) as final_image:
            path = self.filepath
            os.makedirs(dirname(path), exist_ok=True)
            final_image.save(path, format="JPEG", quality=current_app.config["JPEG_QUALITY"])
        return path
