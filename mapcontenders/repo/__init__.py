from abc import ABC, abstractmethod
from typing import Any, List, Optional


class Repo(ABC):
    @abstractmethod
    def get_all(self, **filters) -> List[Any]:
        raise NotImplementedError()

    @abstractmethod
    def get_by_id(self, id: int, raises=True) -> Optional[Any]:
        raise NotImplementedError()
