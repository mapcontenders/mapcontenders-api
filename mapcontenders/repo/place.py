from typing import List, Tuple

from flask import abort
from geoalchemy2 import WKTElement
from sqlalchemy import desc, func

from mapcontenders.const import SRID_CODE
from mapcontenders.model import Pic2PointScore, Point2PicScore, ScoreModel
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel
from mapcontenders.repo import Repo


class PlaceRepo(Repo):
    def get_all(self, **filters) -> List[PlaceModel]:
        """
        Get all places possibly filtered by *difficulty* and *since* (places created after this since)
        """
        difficulty = filters.get("difficulty")
        since = filters.get("since")
        query = PlaceModel.query
        if difficulty is not None:
            query = query.filter(PlaceModel.difficulty == difficulty)
        if since is not None:
            query = query.filter(PlaceModel.creation >= since)
        return query.all()

    def get_by_id(self, id_place: int, raises=True) -> PlaceModel:
        """
        Gets a place by its id
        """
        place = PlaceModel.query.filter(PlaceModel.id == id_place).first()
        if not place and raises:
            abort(404, f"Place #{id_place} not found")
        return place

    def get_random(self, difficulty: int = None) -> PlaceModel:
        """
        Retrieves a random place having specified difficulty (if any is specified)
        """
        query = PlaceModel.query
        if difficulty is not None:
            query = query.filter(PlaceModel.difficulty == difficulty)
        return query.filter(PlaceModel.pictures.any()).order_by(func.random()).first()

    def get_randoms(self, count: int, difficulty: int = None) -> List[PlaceModel]:
        """
        Retrieves *count* random places, having specified difficulty (if any is specified)
        """
        query = PlaceModel.query
        if difficulty is not None:
            query = query.filter(PlaceModel.difficulty == difficulty)
        return query.filter(PlaceModel.pictures.any()).order_by(func.random()).limit(count).all()

    def get_with_distance_by_picture_ids(self, id_pictures: List[int], lon: float, lat: float) -> Tuple[PlaceModel, float]:
        """
        Retrieves a place related to pictures (described by its ids) and computes distance to a lon/lat EPSG:4326 coordinate
        """
        places = (
            PlaceModel.query.join(PictureModel)
            .filter(PictureModel.id.in_(id_pictures))
            .with_entities(
                PlaceModel, func.ST_DistanceSphere(PlaceModel.geom, WKTElement(f"SRID={SRID_CODE};POINT({lon} {lat})", extended=True)).label("distance")
            )
        )

        if not all(p == places[0] for p in places):
            abort(400, "Pictures are not related to the same place")

        place, distance = places.first()
        return place, round(distance / 1000, 3)

    def get_pic2point_scores_by_id(self, id_place: int, count: int) -> List[Pic2PointScore]:
        return Pic2PointScore.query.filter(Pic2PointScore.place_id == id_place).order_by(desc(ScoreModel.submitted)).limit(count).all()

    def get_point2pic_scores_by_id(self, id_place: int, count: int) -> List[Point2PicScore]:
        return Point2PicScore.query.filter(Point2PicScore.place_id == id_place).order_by(desc(Point2PicScore.submitted)).limit(count).all()
