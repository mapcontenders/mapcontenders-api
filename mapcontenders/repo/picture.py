from typing import List

from flask import abort
from sqlalchemy import func

from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel
from mapcontenders.repo import Repo


class PictureRepo(Repo):
    def get_all(self, **filters) -> List[PictureModel]:
        raise NotImplementedError()

    def get_by_id(self, id: int, raises=True) -> PictureModel:
        picture = PictureModel.query.filter(PictureModel.id == id).first()
        if not picture and raises:
            abort(404, f"Picture #{id} not found")
        return picture

    def get_by_ids(self, ids: List[int]) -> List[PictureModel]:
        return PictureModel.query.filter(PictureModel.id.in_(ids)).all()

    def get_randoms(self, count: int, excluded_place_id: int, difficulty: int = None) -> List[PictureModel]:
        """
        Gets some random pictures NOT belonging to the 'excluded_place_id'
        If 'difficulty' is specified, then random pictures belonging to places with the difficulty level
        """
        # TODO: avoid duplicates in following query
        query = PictureModel.query.filter(PictureModel.place_id != excluded_place_id)
        if difficulty is not None:
            query = query.join(PlaceModel).filter(PlaceModel.difficulty == difficulty)
        return list(query.order_by(func.random()).limit(count))
