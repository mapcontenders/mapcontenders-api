import json
import os
import urllib.parse
from typing import Any, Dict

import requests
from flask import current_app, g, render_template, url_for
from flask.views import MethodView
from flask_cors import cross_origin
from flask_smorest import Blueprint
from psycopg2 import sql
from psycopg2.extras import DictCursor
from sqlalchemy import column, func, select

from mapcontenders.const import MVT_SRID_CODE
from mapcontenders.database import db
from mapcontenders.extensions import cache
from mapcontenders.model.place import PlaceModel
from mapcontenders.schema.tiles import (
    MapBoxStyleArgsSchema,
    MvtArgsSchema,
    VectorTilesProtobufResponse,
)

blp = Blueprint("Tiles", __name__, description="Tiles API", static_folder="static")


@blp.route("/tiles/places/<int:z>/<int:x>/<int:y>.mvt")
class MvtView(MethodView):
    @blp.arguments(MvtArgsSchema, location="path", as_kwargs=True)
    @blp.response(200, description="OK")
    @blp.response(204, description="Empty (MapBox SDK requires to send a proper '204 no content' when the response is empty)")
    @cross_origin(max_age=1000)
    # TODO: check buggy redis cache for mvt protobuf (librabbitmq ?)
    # @cache.cached(60)
    def get(self, z: int, x: int, y: int) -> VectorTilesProtobufResponse:
        """Get MVT with SqlAlchemy request"""

        # declare request as tile request (will not be logged)
        g.tile_request = True

        # PostGIS MVT generation using ST_TileEnvelope
        # PostGis' ST_TileEnvelope func returns geometry using EPSG:3857
        # so make sure to generate MVT geoms from geometries also in EPSG:3857
        query = select([func.ST_AsMVT(column("place"), "place", 4096, "geom")]).select_from(
            select(
                [
                    column("id"),
                    column("creation"),
                    column("name"),
                    column("hint"),
                    column("difficulty"),
                    column("likes"),
                    column("dislikes"),
                    func.ST_AsMVTGeom(func.ST_Transform(PlaceModel.geom, MVT_SRID_CODE), func.ST_TileEnvelope(z, x, y), 4096, 256, True).label("geom"),
                ]
            ).alias("place")
        )

        with db.engine.connect() as connection:
            tile = connection.scalar(query)
            return VectorTilesProtobufResponse(tile, 200) if tile else VectorTilesProtobufResponse(status=204)


@blp.route("/tiles/places/pg/<int:z>/<int:x>/<int:y>.mvt")
class MvtPgView(MethodView):
    @blp.arguments(MvtArgsSchema, location="path", as_kwargs=True)
    @blp.response(200, description="OK")
    @blp.response(204, description="Empty (MapBox SDK requires to send a proper '204 no content' when the response is empty)")
    @cross_origin(max_age=1000)
    # TODO: check buggy redis cache for mvt protobuf (librabbitmq ?)
    # @cache.cached(60)
    def get(self, z: int, x: int, y: int) -> VectorTilesProtobufResponse:
        """Get MVT with raw PostGis request"""

        # declare request as tile request (will not be logged)
        g.tile_request = True

        query = sql.SQL(
            """
            WITH
            p AS (
                SELECT
                    pl.id, pl.creation, pl.name, pl.hint, pl.difficulty, pl.likes, pl.dislikes,
                    ST_AsMVTGeom(ST_Transform(pl.geom, %(srid)s), ST_TileEnvelope(%(z)s, %(x)s, %(y)s), 4096, 256, True) AS geom
                FROM place pl
            )
            SELECT ST_AsMVT(p, 'place', 4096, 'geom') AS tile FROM p
            """
        )

        with db.engine.raw_connection().connection as connection, connection.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(query, dict(z=z, x=x, y=y, srid=MVT_SRID_CODE))
            tile = bytes(cursor.fetchone().get("tile"))
            return VectorTilesProtobufResponse(tile, 200) if tile else VectorTilesProtobufResponse(status=204)


@blp.route("/mapbox/styles/qgis.json")
class MapBoxBasicStyleView(MethodView):
    @blp.arguments(MapBoxStyleArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, description="OK")
    @cross_origin(max_age=1000)
    @cache.cached(60 * 30)
    def get(self, **kwargs) -> Dict[str, Any]:
        """Get basic MapBox style for places (compatible with QGIS)"""
        minzoom = kwargs.get("minzoom")
        maxzoom = kwargs.get("maxzoom")

        # load default mapbox json style
        with open(os.path.join("mapcontenders", "mapbox", "style-qgis.json"), "r") as f:
            style = json.load(f)

            # set vector source
            tiles = url_for("Tiles.MvtView", _external=True, x=-1, y=-2, z=-3).replace("-1", "{x}", 1).replace("-2", "{y}", 1).replace("-3", "{z}", 1)
            source = {"type": "vector", "tiles": [tiles]}
            if minzoom is not None:
                source["minzoom"] = minzoom
            if maxzoom is not None:
                source["maxzoom"] = maxzoom
            style["sources"]["mapcontenders"] = source

            # set sprites url
            parsed = urllib.parse.urlparse(tiles)
            style["sprite"] = f"{parsed.scheme}://{parsed.netloc}{url_for('static', filename='mc-sprite')}"

            return style


@blp.route("/mapbox/styles/mapbox.json")
class MapBoxElaborateStyleView(MethodView):
    @blp.arguments(MapBoxStyleArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, description="OK")
    @cross_origin(max_age=1000)
    @cache.cached(60 * 30)
    def get(self, **kwargs) -> Dict[str, Any]:
        """Get MapBox style for places (pretty version for MapBox JS)'"""

        minzoom = kwargs.get("minzoom")
        maxzoom = kwargs.get("maxzoom")

        # get basic klokantech map style
        style = requests.get(current_app.config["MAPBOX_BASE_STYLE"]).json()

        # set vector source
        tiles = url_for("Tiles.MvtView", _external=True, x=-1, y=-2, z=-3).replace("-1", "{x}", 1).replace("-2", "{y}", 1).replace("-3", "{z}", 1)
        source = {"type": "vector", "tiles": [tiles]}
        if minzoom is not None:
            source["minzoom"] = minzoom
        if maxzoom is not None:
            source["maxzoom"] = maxzoom
        style["sources"]["mapcontenders"] = source

        # set sprites url
        parsed = urllib.parse.urlparse(tiles)
        style["sprite"] = f"{parsed.scheme}://{parsed.netloc}{url_for('static', filename='mc-sprite')}"

        # add mapcontenders layers
        with open(os.path.join("mapcontenders", "mapbox", "style-qgis.json"), "r") as f:
            qgis = json.load(f)
            for layer in qgis["layers"]:
                style["layers"].append(layer)

        # TODO add more stuff like label layers
        return style


@blp.route("/mapbox/qm")
class MapBoxViewerView(MethodView):
    def get(self):
        return render_template("map.html")
