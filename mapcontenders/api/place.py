from typing import Any, Dict, List

from flask import abort, current_app
from flask.views import MethodView
from flask_smorest import Blueprint
from geoalchemy2.shape import from_shape
from PIL import Image
from shapely.geometry import Point

from mapcontenders.database import db
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel
from mapcontenders.repo.place import PlaceRepo
from mapcontenders.schema.pictures import PictureSchema, UploadPictureSchema
from mapcontenders.schema.places import (
    CreatePlaceSchema,
    PlaceByIdSchema,
    PlaceFeatureSchema,
    PlaceSchema,
    PlaceWithPicturesSchema,
)

blp = Blueprint("Place", __name__, description="Place API")


@blp.route("/place")
class PlaceView(MethodView):
    @blp.arguments(CreatePlaceSchema, as_kwargs=True)
    @blp.response(200, schema=PlaceSchema)
    def post(self, **kwargs) -> PlaceModel:
        """Create a new place"""
        kwargs["geom"] = from_shape(Point(kwargs.pop("lon"), kwargs.pop("lat")))
        place = PlaceModel(**kwargs)
        db.session.add(place)
        db.session.commit()
        return place


@blp.route("/place/<int:id_place>")
class PlaceByIdView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PlaceWithPicturesSchema)
    def get(self, id_place: int) -> PlaceModel:
        """Get place by id"""
        return PlaceRepo().get_by_id(id_place)


@blp.route("/place/<int:id_place>/pictures")
class PlacePicturesView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PictureSchema(many=True))
    def get(self, id_place: int) -> List[PictureModel]:
        """Get place pictures by id"""
        return PlaceRepo().get_by_id(id_place).pictures

    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.arguments(UploadPictureSchema, location="files", as_kwargs=True)
    @blp.response(200, schema=PictureSchema)
    def post(self, **kwargs) -> PictureModel:
        """Upload a new picture for place"""

        picture_file = kwargs.pop("picture_file")
        allowed_mimes = current_app.config["ALLOWED_PICTURES_MIMETYPES"]
        if picture_file.content_type not in allowed_mimes:
            abort(400, f"Incorrect content_type '{picture_file.content_type}'. Allowed types: {allowed_mimes}")

        place = PlaceRepo().get_by_id(kwargs.pop("id_place"))
        picture = PictureModel(place_id=place.id)
        db.session.add(picture)
        db.session.commit()
        with Image.open(picture_file) as image:
            picture.save_on_filesystem(image)
        return picture


@blp.route("/place/<int:id_place>.geojson")
class PlaceGeojsonByIdView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PlaceFeatureSchema)
    def get(self, id_place: int) -> Dict[str, Any]:
        """Get place geojson by id"""
        place = PlaceRepo().get_by_id(id_place)
        return {"geometry": place.geom, "properties": place}


@blp.route("/place/<int:id_place>/like")
class PlaceLikeByIdView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PlaceWithPicturesSchema)
    def post(self, id_place: int) -> PlaceModel:
        """Add a like to place"""
        place = PlaceRepo().get_by_id(id_place)
        db.session.add(place)
        place.likes += 1
        db.session.commit()
        return place


@blp.route("/place/<int:id_place>/dislike")
class PlaceDislikeByIdView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PlaceWithPicturesSchema)
    def post(self, id_place: int) -> PlaceModel:
        """Add a dislike to place"""
        place = PlaceRepo().get_by_id(id_place)
        db.session.add(place)
        place.dislikes += 1
        db.session.commit()
        return place
