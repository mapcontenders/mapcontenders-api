from typing import Dict

from flask import current_app
from flask.views import MethodView
from flask_smorest import Blueprint

from mapcontenders.extensions import cache
from mapcontenders.schema.misc import ApiTitleSchema, ApiVersionSchema

blp = Blueprint("Misc", __name__, description="Misc endpoints")


@blp.route("/title")
class ApiTitleView(MethodView):
    @blp.response(200, schema=ApiTitleSchema)
    @cache.cached(60 * 30)
    def get(self) -> Dict:
        """API title"""
        return dict(title=current_app.config["API_TITLE"])


@blp.route("/version")
class ApiVersionView(MethodView):
    @blp.response(200, schema=ApiVersionSchema)
    @cache.cached(60 * 30)
    def get(self) -> Dict:
        """API version"""
        return dict(version=current_app.config["API_VERSION"])
