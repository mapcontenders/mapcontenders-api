from typing import Any, Dict, List

from flask.views import MethodView
from flask_smorest import Blueprint

from mapcontenders.model.place import PlaceModel
from mapcontenders.repo.place import PlaceRepo
from mapcontenders.schema.places import (
    PlaceQueryFilterSchema,
    PlaceSchema,
    PlacesFeatureCollectionSchema,
)

blp = Blueprint("Places", __name__, description="Places API")


@blp.route("/places")
class PlacesView(MethodView):
    @blp.arguments(PlaceQueryFilterSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=PlaceSchema(many=True))
    def get(self, **kwargs) -> List[PlaceModel]:
        """Get all places possibly filtered"""
        return PlaceRepo().get_all(**kwargs)


@blp.route("/places.geojson")
class PlacesGeojsonView(MethodView):
    @blp.arguments(PlaceQueryFilterSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=PlacesFeatureCollectionSchema)
    def get(self, **kwargs) -> Dict[str, Any]:
        """Get all places as geojson"""
        return {"features": [{"geometry": p.geom, "properties": p} for p in PlaceRepo().get_all(**kwargs)]}
