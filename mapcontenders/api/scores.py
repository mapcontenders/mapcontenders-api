from typing import List

from flask.views import MethodView
from flask_smorest import Blueprint

from mapcontenders.model import Pic2PointScore, Point2PicScore
from mapcontenders.repo.place import PlaceRepo
from mapcontenders.schema.places import PlaceByIdSchema
from mapcontenders.schema.scores import (
    ScorePic2PointSchema,
    ScorePoint2PicSchema,
    ScoresCountArgsSchema,
)

blp = Blueprint("Score", __name__, description="Score API")


@blp.route("/scores/<int:id_place>/pic2point")
class Pic2PointScoresByPlaceView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.arguments(ScoresCountArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=ScorePic2PointSchema(many=True))
    def get(self, id_place: int, count: int) -> List[Pic2PointScore]:
        return PlaceRepo().get_pic2point_scores_by_id(id_place, count)


@blp.route("/scores/<int:id_place>/point2pic")
class Point2PicScoresByPlaceView(MethodView):
    @blp.arguments(PlaceByIdSchema, location="path", as_kwargs=True)
    @blp.arguments(ScoresCountArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=ScorePoint2PicSchema(many=True))
    def get(self, id_place: int, count: int) -> List[Point2PicScore]:
        return PlaceRepo().get_point2pic_scores_by_id(id_place, count)
