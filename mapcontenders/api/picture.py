import os

from flask import Response, abort, send_file
from flask.views import MethodView
from flask_smorest import Blueprint

from mapcontenders.model.picture import PictureModel
from mapcontenders.repo.picture import PictureRepo
from mapcontenders.schema.pictures import PictureByIdSchema, PictureWithPlaceSchema

blp = Blueprint("Picture", __name__, description="Picture API")


@blp.route("/picture/<int:id_picture>")
class PictureByIdView(MethodView):
    @blp.arguments(PictureByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, schema=PictureWithPlaceSchema)
    def get(self, id_picture: int) -> PictureModel:
        """Get picture by id"""
        return PictureRepo().get_by_id(id_picture)


@blp.route("/picture/<string:id_picture>.jpg")
class PictureByIdDataView(MethodView):
    @blp.arguments(PictureByIdSchema, location="path", as_kwargs=True)
    @blp.response(200, description="OK (image in JPEG format)")
    # TODO: check buggy redis cache for picture files
    # @cache.cached(timeout=60*15)
    def get(self, id_picture: int) -> Response:
        """Get image data in JPEG format"""
        picture = PictureRepo().get_by_id(id_picture)
        if not os.path.exists(picture.filepath):
            abort(404, "Image not found")
        # hack: send_file is actually executed in parent folder
        return send_file(os.path.join("..", picture.filepath), mimetype="image/jpeg")
