import logging
from time import time

import httpagentparser
from flask import Flask, Response, g, jsonify, make_response, request
from flask_smorest import Api
from werkzeug.exceptions import HTTPException

from mapcontenders.api.misc import blp as blp_misc
from mapcontenders.api.pic2point import blp as blp_pic2point
from mapcontenders.api.picture import blp as blp_picture
from mapcontenders.api.place import blp as blp_place
from mapcontenders.api.places import blp as blp_places
from mapcontenders.api.point2pic import blp as blp_point2pic
from mapcontenders.api.scores import blp as blp_scores
from mapcontenders.api.tiles import blp as blp_tiles

PREFIX = "/api"


def register_hooks(app: Flask) -> None:
    @app.before_request
    def before_request() -> None:
        g.start_time = time()

    @app.after_request
    def after_request(response: Response) -> Response:

        # tiles request are not logged
        if g.get("tile_request", False):
            return response

        elapsed = time() - g.start_time

        # try parse user agent
        ua = request.user_agent.string
        parsed = httpagentparser.detect(ua)
        if "os" in parsed and "browser" in parsed:
            os, browser = httpagentparser.simple_detect(ua)
            ua = f"{browser} on {os}"

        msg = "[{ip}] {method} {uri} -> {code} t: {time}s (agent: {user_agent})".format(
            ip=request.headers.getlist("X-Forwarded-For")[0] if request.headers.getlist("X-Forwarded-For") else request.remote_addr,
            method=request.method,
            uri=request.path,
            code=response.status_code,
            time=round(elapsed, 3),
            user_agent=ua,
        )
        logging.getLogger(__name__).info(msg)
        return response


def register_error_handlers(app: Flask) -> None:
    @app.errorhandler(HTTPException)
    def handle_error(err: HTTPException) -> Response:
        logging.getLogger(__name__).error(err)
        payload = dict(code=err.code, status=err.name, message=err.description)
        return make_response(jsonify(payload), err.code)


def register_api(app: Flask) -> Api:
    api = Api(app)
    api.register_blueprint(blp_misc, url_prefix=PREFIX)
    api.register_blueprint(blp_places, url_prefix=PREFIX)
    api.register_blueprint(blp_place, url_prefix=PREFIX)
    api.register_blueprint(blp_picture, url_prefix=PREFIX)
    api.register_blueprint(blp_pic2point, url_prefix=PREFIX)
    api.register_blueprint(blp_point2pic, url_prefix=PREFIX)
    api.register_blueprint(blp_scores, url_prefix=PREFIX)
    api.register_blueprint(blp_tiles)
    return api
