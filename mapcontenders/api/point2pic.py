import random
from datetime import datetime
from random import randrange

from flask import abort
from flask.views import MethodView
from flask_smorest import Blueprint

from mapcontenders.database import db
from mapcontenders.model import Point2PicScore
from mapcontenders.model.point2pic import Point2PicGuessSet
from mapcontenders.repo.picture import PictureRepo
from mapcontenders.repo.place import PlaceRepo
from mapcontenders.schema.guess import (
    GuessArgsSchema,
    Point2PicGuessSetSchema,
    Point2PicGuessTrySchema,
)
from mapcontenders.schema.scores import ScorePoint2PicSchema

blp = Blueprint("Guess point2pic", __name__, description="Guess point to picture API")


@blp.route("/guess/point2pic")
class GuessOneshotPointToPictureView(MethodView):
    @blp.arguments(GuessArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=Point2PicGuessSetSchema)
    def get(self, **kwargs) -> Point2PicGuessSet:
        """
        Get a point2pic guess set
        """
        difficulty = kwargs.pop("difficulty")
        # get a random place possibly filtered by required difficulty
        place = PlaceRepo().get_random(difficulty)
        # number of pictures to send (depending on difficulty)
        picture_count = difficulty if difficulty else place.difficulty
        # get some random wrong pictures not related to the place to guess
        pictures = PictureRepo().get_randoms(picture_count, place.id, difficulty)
        # append correct picture to list
        pictures.append(place.pictures[randrange(place.pictures_count)])
        # shuffle pictures
        random.shuffle(pictures)
        return Point2PicGuessSet(difficulty=difficulty, place=place, pictures=pictures)

    @blp.arguments(Point2PicGuessTrySchema, as_kwargs=True)
    @blp.response(200, schema=ScorePoint2PicSchema)
    def post(self, **kwargs) -> Point2PicScore:
        """
        Try a point2pic guess and get result
        """

        id_place = kwargs.pop("id_place")
        id_pictures, guessed_id_picture = kwargs.pop("id_pictures"), kwargs.pop("guessed_id_picture")

        if guessed_id_picture not in id_pictures:
            abort(400, "Guessed picture is not in the choice")

        # get place to guess and picture choices
        place_to_guess = PlaceRepo().get_by_id(id_place)
        picture_choices = PictureRepo().get_by_ids(id_pictures)
        # get place related to guessed picture
        good_picture = next(p for p in picture_choices if p.place_id == id_place)
        guessed_picture = PictureRepo().get_by_id(guessed_id_picture)
        success = id_place == guessed_picture.place_id

        difficulty = kwargs.pop("difficulty")
        now = datetime.now()
        duration = round((now - kwargs.pop("created")).total_seconds())

        # result = Point2PicGuessResult(
        #     duration=duration,
        #     difficulty=difficulty,
        #     place=place_to_guess,
        #     pictures=picture_choices,
        #     guessed_picture=guessed_picture,
        #     correct_picture=good_picture,
        #     success=success,
        # )

        score = Point2PicScore(
            difficulty=difficulty,
            submitted=now,
            duration=duration,
            place_id=place_to_guess.id,
            pictures=picture_choices,
            success=success,
            guessed_picture_id=guessed_picture.id,
        )
        db.session.add(score)
        db.session.commit()

        return score
