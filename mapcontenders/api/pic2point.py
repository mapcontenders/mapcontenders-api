import random
from datetime import datetime

from flask.views import MethodView
from flask_smorest import Blueprint
from geoalchemy2.shape import from_shape
from shapely.geometry import Point

from mapcontenders.database import db
from mapcontenders.model import Pic2PointScore
from mapcontenders.model.pic2point import Pic2PointGuessSet
from mapcontenders.model.place import MAX_DIFF
from mapcontenders.repo.place import PlaceRepo
from mapcontenders.schema.guess import (
    GuessArgsSchema,
    Pic2PointGuessSetSchema,
    Pic2PointGuessTrySchema,
)
from mapcontenders.schema.scores import ScorePic2PointSchema

blp = Blueprint("Guess pic2point", __name__, description="Guess picture to point API")


@blp.route("/guess/pic2point")
class GuessOneshotPictureToPointView(MethodView):
    @blp.arguments(GuessArgsSchema, location="query", as_kwargs=True)
    @blp.response(200, schema=Pic2PointGuessSetSchema)
    def get(self, **kwargs) -> Pic2PointGuessSet:
        """
        Get a pic2point guess set
        """
        difficulty = kwargs.pop("difficulty")
        place = PlaceRepo().get_random(difficulty)
        # shuffle pictures
        pics = list(place.pictures)
        random.shuffle(pics)
        # limit number of pictures sent
        # depends on difficulty: easy(1) -> max 3 pics, medium(2) -> max 2 pics, hard(3) -> max 1 pic
        pics = pics[: MAX_DIFF - place.difficulty + 1]
        return Pic2PointGuessSet(difficulty=place.difficulty, pictures=pics)

    @blp.arguments(Pic2PointGuessTrySchema, as_kwargs=True)
    @blp.response(200, schema=ScorePic2PointSchema)
    def post(self, **kwargs) -> Pic2PointScore:
        """
        Try a pic2point guess and get result
        """
        lon, lat = kwargs.pop("guessed_lon"), kwargs.pop("guessed_lat")
        # get place related to pictures and compute distance to contende's location guess
        place, distance = PlaceRepo().get_with_distance_by_picture_ids(kwargs.pop("id_pictures"), lon, lat)

        difficulty = kwargs.pop("difficulty")
        now = datetime.now()
        duration = round((now - kwargs.pop("created")).total_seconds())

        # result = Pic2PointGuessResult(
        #     duration=duration,
        #     difficulty=difficulty,
        #     pictures=place.pictures,
        #     guessed_lon=lon,
        #     guessed_lat=lat,
        #     place=place,
        #     distance=distance,
        # )

        score = Pic2PointScore(
            difficulty=difficulty,
            submitted=now,
            duration=duration,
            place_id=place.id,
            pictures=place.pictures,
            distance=distance,
            guessed_geom=from_shape(Point(lon, lat)),
        )
        db.session.add(score)
        db.session.commit()

        return score
