import logging
import os
from distutils.dir_util import copy_tree

import click
from flask import Flask
from flask.cli import with_appcontext
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text

db: SQLAlchemy = SQLAlchemy()
migrate: Migrate = Migrate()


@click.command("populate_db")
@with_appcontext
def populate_db() -> None:

    # copy initial pictures
    source = os.path.join("db", "pictures")
    dest = os.environ.get("PICTURES_FOLDER", "pictures")
    logging.getLogger(__name__).info(f"Copying initial pictures from '{source}' to '{dest}'")
    copy_tree(source, dest)

    # run init sql
    with open(os.path.join("db", "init.sql"), "r") as sql:
        engine = db.get_engine()
        escaped = text(sql.read())
        engine.execute(escaped)
        logging.getLogger(__name__).info("Database initialization done")


def register_database(app: Flask) -> None:
    db.init_app(app)
    app.cli.add_command(populate_db)
    migrate.init_app(app, db)
