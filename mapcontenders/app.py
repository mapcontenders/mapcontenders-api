import logging
import os

from flask import Flask

import settings
from mapcontenders.api import register_api, register_error_handlers, register_hooks
from mapcontenders.database import register_database
from mapcontenders.extensions import register_extensions
from mapcontenders.schema import register_fields


def create_app(config_object=settings) -> Flask:
    """
    WSGI App factory as per the Flask app factory pattern http://flask.pocoo.org/docs/1.0/patterns/appfactories/
    """
    logging.getLogger(__name__).info(f"Worker boot [pid={os.getpid()}]")
    app = Flask(__name__)
    app.config.from_object(config_object)
    register_extensions(app)
    register_database(app)
    register_hooks(app)
    register_error_handlers(app)
    api = register_api(app)
    register_fields(api)
    return app
