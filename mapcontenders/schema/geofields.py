from typing import Any, Mapping, Optional

from geoalchemy2.shape import from_shape, to_shape
from marshmallow import ValidationError, fields
from shapely.geometry import mapping, shape
from shapely.geometry.base import BaseGeometry

from mapcontenders.const import SRID_CODE


class GeometryField(fields.Field):
    @property
    def __type__(self) -> Optional[str]:
        return None

    def _serialize(self, value: Any, attr: str, obj: Any, **kwargs) -> Any:
        if value is None:
            return None
        try:
            return mapping(to_shape(value))
        except ValueError as error:
            raise ValidationError("Invalid geometry") from error

    def _deserialize(self, value: Any, attr: Optional[str], data: Optional[Mapping[str, Any]], **kwargs) -> BaseGeometry:
        if value is None:
            return None
        if "type" not in value or (self.__type__ is not None and value["type"] != self.__type__):
            raise ValidationError("Wrong GeoJSON Type")
        try:
            return from_shape(shape(value), srid=SRID_CODE)
        except ValueError as error:
            raise ValidationError("Invalid GeoJSON") from error


class PointField(GeometryField):
    @property
    def __type__(self) -> Optional[str]:
        return "Point"


class LineStringField(GeometryField):
    @property
    def __type__(self) -> Optional[str]:
        return "LineString"


class PolygonField(GeometryField):
    @property
    def __type__(self) -> Optional[str]:
        return "Polygon"
