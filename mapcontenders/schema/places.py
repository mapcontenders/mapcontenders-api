from marshmallow import Schema, fields, validate

from mapcontenders.const import SRID
from mapcontenders.model.place import MAX_DIFF, MIN_DIFF
from mapcontenders.schema import (
    DESC_DIFFICULTY,
    GeojsonFeatureCollectionSchema,
    GeojsonFeatureSchema,
    PointField,
)
from mapcontenders.schema.pictures import PictureSchema


class PlaceByIdSchema(Schema):
    id_place = fields.Integer(metadata=dict(description="Place id"))


class PlaceQueryFilterSchema(Schema):
    difficulty = fields.Integer(
        required=False,
        validate=validate.Range(min=MIN_DIFF, max=MAX_DIFF),
        metadata=dict(description="Filter with selected difficulty level (1=easy, 2=medium, 3=hard)"),
    )
    since = fields.DateTime(required=False, metadata=dict(description="Filter with min creation date"))


class CreatePlaceSchema(Schema):
    name = fields.String(metadata=dict(description="Place name"))
    hint = fields.String(required=False, load_default=None, metadata=dict(description="Guess hint"))
    difficulty = fields.Integer(validate=validate.Range(min=MIN_DIFF, max=MAX_DIFF), metadata=dict(description=DESC_DIFFICULTY))
    creator = fields.String(metadata=dict(description="Place creator"))
    lon = fields.Float(metadata=dict(description=f"Longitude ({SRID})"))
    lat = fields.Float(metadata=dict(description=f"Latitude ({SRID})"))


class PlaceSchema(Schema):
    id = fields.Integer(metadata=dict(description="Place id"))

    name = fields.String(metadata=dict(description="Place name"))
    hint = fields.String(metadata=dict(description="Guess hint"))
    difficulty = fields.Integer(validate=validate.Range(min=MIN_DIFF, max=MAX_DIFF), metadata=dict(description=DESC_DIFFICULTY))
    creation = fields.DateTime(metadata=dict(description="Place creation datetime"))
    creator = fields.String(metadata=dict(description="Place creator"))

    lon = fields.Float(metadata=dict(description=f"Longitude ({SRID})"))
    lat = fields.Float(metadata=dict(description=f"Latitude ({SRID})"))

    likes = fields.Integer(metadata=dict(description="Likes count"))
    dislikes = fields.Integer(metadata=dict(description="Dislikes count"))

    pictures_count = fields.Integer(metadata=dict(description="Registered pictures count"))


class PlaceWithPicturesSchema(PlaceSchema):
    pictures = fields.List(fields.Nested(PictureSchema), metadata=dict(description="Place pictures"))


class PlaceFeatureSchema(GeojsonFeatureSchema):
    geometry = PointField(metadata=dict(description=f"Place point geometry using SRID '{SRID}'"))
    properties = fields.Nested(PlaceSchema, metadata=dict(description="Place properties"))


class PlacesFeatureCollectionSchema(GeojsonFeatureCollectionSchema):
    features = fields.List(fields.Nested(PlaceFeatureSchema), metadata=dict(description="Place features"))
