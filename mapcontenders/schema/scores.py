from marshmallow import Schema, fields, validate

from mapcontenders.model.place import MAX_DIFF, MIN_DIFF
from mapcontenders.schema import DESC_DIFFICULTY
from mapcontenders.schema.places import PlaceSchema


class ScoresCountArgsSchema(Schema):
    count = fields.Integer(validate=validate.Range(min=1, max=10), load_default=5, metadata=dict(description="Count of scores to get"))


class ScoreSchema(Schema):
    submitted = fields.DateTime(metadata=dict(description="Guess datetime"))
    difficulty = fields.Integer(
        required=False,
        load_default=None,
        validate=validate.Range(min=MIN_DIFF, max=MAX_DIFF),
        metadata=dict(description=DESC_DIFFICULTY),
    )
    duration = fields.Integer(metadata=dict(description="Guess duration"))
    place = fields.Nested(PlaceSchema, metadata=dict(description="Place to guess"))


class ScorePic2PointSchema(ScoreSchema):
    distance = fields.Float(validate=validate.Range(min=0), metadata=dict(description="Distance to place"))


class ScorePoint2PicSchema(ScoreSchema):
    success = fields.Boolean(metadata=dict(description="If place picture was correctly guessed"))
