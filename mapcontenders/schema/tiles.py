from flask import Response
from marshmallow import Schema, fields, validate


class MvtArgsSchema(Schema):
    z = fields.Integer(required=True, metadata=dict(description="Tile z"))
    x = fields.Integer(required=True, metadata=dict(description="Tile x"))
    y = fields.Integer(required=True, metadata=dict(description="Tile y"))


class VectorTilesProtobufResponse(Response):
    default_mimetype = "application/x-protobuf"


class MapBoxStyleArgsSchema(Schema):
    minzoom = fields.Integer(required=False, validate=validate.Range(min=1, max=20), metadata=dict(description="Min zoom"))
    maxzoom = fields.Integer(required=False, validate=validate.Range(min=1, max=20), metadata=dict(description="Min zoom"))
