from flask import url_for
from flask_smorest.fields import Upload
from marshmallow import Schema, fields


class PictureByIdSchema(Schema):
    id_picture = fields.Integer(metadata=dict(description="Picture id"))


class UploadPictureSchema(Schema):
    picture_file = Upload(metadata=dict(description="Picture file (JPEG or PNG format)"))


class PictureSchema(Schema):
    id = fields.Integer(metadata=dict(description="Picture id"))
    creation = fields.DateTime(metadata=dict(description="Picture creation datetime"))
    url = fields.Function(
        serialize=lambda p: url_for("Picture.PictureByIdDataView", _external=True, id_picture=p.id),
        metadata=dict(description="Picture image browsable URL"),
    )


class PictureWithPlaceSchema(PictureSchema):
    place_id = fields.Integer(metadata=dict(description="Place id"))
