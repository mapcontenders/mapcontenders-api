import json
import os

from flask_smorest import Api
from jsonschema import Draft4Validator, RefResolver
from marshmallow import Schema, fields, validate

from mapcontenders.schema.geofields import (
    GeometryField,
    LineStringField,
    PointField,
    PolygonField,
)

DESC_DIFFICULTY = "Difficulty level (1=easy, 2=medium, 3=hard)"


def register_fields(api: Api) -> None:
    api.register_field(GeometryField, "GeoJSON", None)
    api.register_field(PointField, "GeoJSON <Point>", None)
    api.register_field(LineStringField, "GeoJSON <LineString>", None)
    api.register_field(PolygonField, "GeoJSON <Polygon>", None)


class GeojsonFeatureSchema(Schema):
    type = fields.String(validate=validate.Equal("Feature"), dump_default="Feature")
    geometry = GeometryField()
    properties = fields.Nested(fields.Dict, dump_default={})


class GeojsonFeatureCollectionSchema(Schema):
    type = fields.String(validate=validate.Equal("FeatureCollection"), dump_default="FeatureCollection")
    features = fields.List(fields.Nested(GeojsonFeatureSchema))


def geojson_validator() -> Draft4Validator:

    schema_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), "geojson")

    geojson_base = json.load(open(os.path.join(schema_folder, "geojson.json")))

    # pre-cache the associated schema data so that we don't have to connect to the the given URLs (which don't exist)
    cached_json = {
        "https://json-schema.org/geojson/crs.json": json.load(open(os.path.join(schema_folder, "crs.json"))),
        "https://json-schema.org/geojson/bbox.json": json.load(open(os.path.join(schema_folder, "bbox.json"))),
        "https://json-schema.org/geojson/geometry.json": json.load(open(os.path.join(schema_folder, "geometry.json"))),
    }
    resolver = RefResolver("https://json-schema.org/geojson/geojson.json", geojson_base, store=cached_json)

    return Draft4Validator(geojson_base, resolver=resolver)
