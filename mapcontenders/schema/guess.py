from marshmallow import Schema, fields, validate

from mapcontenders.const import SRID
from mapcontenders.model.place import MAX_DIFF, MIN_DIFF
from mapcontenders.schema import DESC_DIFFICULTY
from mapcontenders.schema.pictures import PictureSchema
from mapcontenders.schema.places import PlaceSchema

DESC_SET_CREATION = "Set creation datetime"


class GuessArgsSchema(Schema):
    difficulty = fields.Integer(load_default=None, validate=validate.Range(min=MIN_DIFF, max=MAX_DIFF), metadata=dict(description=DESC_DIFFICULTY))


# region pic2point


class Pic2PointGuessSetSchema(Schema):
    created = fields.DateTime(metadata=dict(description=DESC_SET_CREATION))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    pictures = fields.List(fields.Nested(PictureSchema), metadata=dict(description="Pictures from place to guess"))


class Pic2PointGuessTrySchema(Schema):
    created = fields.DateTime(metadata=dict(description=DESC_SET_CREATION))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    id_pictures = fields.List(fields.Integer, metadata=dict(description="Pictures from place to guess (IDs)"))
    guessed_lon = fields.Float(metadata=dict(description=f"Longitude guessed by contender ({SRID})"))
    guessed_lat = fields.Float(metadata=dict(description=f"Latitude guessed by contender ({SRID})"))


class Pic2PointGuessResultSchema(Schema):
    duration = fields.Integer(metadata=dict(description="Guess set duration (seconds)"))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    pictures = fields.List(fields.Nested(PictureSchema), metadata=dict(description="Pictures from place to guess"))
    guessed_lon = fields.Float(metadata=dict(description=f"Longitude guessed by contender ({SRID})"))
    guessed_lat = fields.Float(metadata=dict(description=f"Latitude guessed by contender ({SRID})"))
    place = fields.Nested(PlaceSchema, metadata=dict(description="Place to guess"))
    distance = fields.Float(metadata=dict(description="Distance between contender's guess and place real location (km)"))


# endregion


# region point2pic


class Point2PicGuessSetSchema(Schema):
    created = fields.DateTime(metadata=dict(description=DESC_SET_CREATION))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    place = fields.Nested(PlaceSchema, metadata=dict(description="Place for which correct picture guess"))
    pictures = fields.List(fields.Nested(PictureSchema), metadata=dict(description="Picture choices"))


class Point2PicGuessTrySchema(Schema):
    created = fields.DateTime(metadata=dict(description=DESC_SET_CREATION))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    id_place = fields.Integer(metadata=dict(description="Place for which contender has to guess picture (ID)"))
    id_pictures = fields.List(fields.Integer, metadata=dict(description="Pictures choices (IDs)"))
    guessed_id_picture = fields.Integer(metadata=dict(description="Picture guessed by contender (ID)"))


class Point2PicGuessResultSchema(Schema):
    duration = fields.Integer(metadata=dict(description="Guess set duration (seconds)"))
    difficulty = fields.Integer(load_default=None, metadata=dict(description=DESC_DIFFICULTY))
    place = fields.Nested(PlaceSchema, metadata=dict(description="Place for which contender had to guess correct picture"))
    pictures = fields.List(fields.Nested(PictureSchema), metadata=dict(description="Pictures choices"))
    guessed_picture = fields.Nested(PictureSchema, metadata=dict(description="Picture guessed by contender"))
    correct_picture = fields.Nested(PictureSchema, metadata=dict(description="Correct picture (answer)"))
    success = fields.Boolean(metadata=dict(description="If correct picture was guessed (result)"))


# endregion
