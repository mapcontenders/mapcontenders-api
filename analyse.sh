#! /bin/bash

export PROJECT=mapcontenders-api

sonar-scanner \
  -Dsonar.hostUrl="$SONAR_HOST" \
  -Dsonar.projectName="$PROJECT" \
  -Dsonar.projectKey="$PROJECT" \
  -Dsonar.projectVersion="$API_VERSION" \
  -Dsonar.login="$SONAR_TOKEN" \
  -Dsonar.sources=mapcontenders \
  -Dsonar.tests=tests \
  -Dsonar.python.coverage.reportPaths=.coverage.xml \
  -Dsonar.qualitygate.wait=true
