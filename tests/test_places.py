from flask import Response
from flask.testing import FlaskClient

from tests.data.data import create_multiple_places, create_place_body


def test_create_place(client: FlaskClient):
    body = create_place_body(name="Test place", hint="Test hint", difficulty=2, lon=1, lat=1, creator="JulienLepers")
    rget: Response = client.post("/api/place", json=body)
    assert rget.status_code == 200
    created_id = rget.get_json()["id"]
    assert created_id == 1

    rpost: Response = client.get(f"/api/place/{created_id}")
    assert rpost.status_code == 200
    data = rpost.get_json()
    assert data["id"] == 1
    assert data["name"] == "Test place"
    assert data["hint"] == "Test hint"
    assert data["difficulty"] == 2
    assert data["lon"] == 1
    assert data["lat"] == 1
    assert data["creator"] == "JulienLepers"


def test_create_multiple_places(client: FlaskClient):
    for i in range(1, 20):
        rget: Response = client.post("/api/place", json=create_place_body(i))
        assert rget.status_code == 200
        assert rget.get_json()["id"] == i


def test_get_places(client: FlaskClient):
    rng = range(1, 21)
    create_multiple_places(client, rng)
    r: Response = client.get("/api/places")
    assert r.status_code == 200
    data = r.get_json()
    assert len(data) == 20
    assert [p["id"] for p in data] == list(rng)
    assert [p["name"] for p in data] == [f"test-name-{i}" for i in rng]
