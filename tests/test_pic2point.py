import random

from flask import Response
from flask.testing import FlaskClient

from mapcontenders.model.place import MAX_DIFF
from tests.data.data import create_dataset


def test_get_pic2point_set(client: FlaskClient):
    create_dataset(client, 2, lambda p: 2)
    r: Response = client.get("/api/guess/pic2point")
    assert r.status_code == 200


def test_multiple_pic2point_with_diff_correct_pictures(client: FlaskClient):
    create_dataset(client, 20, lambda p: random.randint(3, 6))

    for i in range(50):
        difficulty = (i % 3) + 1
        data = client.get(f"/api/guess/pic2point?difficulty={difficulty}").get_json()
        assert data["difficulty"] == difficulty

        # check the correct number of provided pictures (depends on difficulty)
        pictures = data["pictures"]
        assert len(pictures) >= 1
        assert len(pictures) == MAX_DIFF - difficulty + 1
