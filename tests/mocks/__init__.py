from datetime import datetime

from geoalchemy2.shape import from_shape
from shapely.geometry import Point

from mapcontenders.const import SRID_CODE
from mapcontenders.model.picture import PictureModel
from mapcontenders.model.place import PlaceModel

now = datetime.now()

MOCK_GET_ALL_PLACES = "mapcontenders.repo.place.PlaceRepo.get_all"
MOCK_GET_PLACE_BY_ID = "mapcontenders.repo.place.PlaceRepo.get_by_id"
MOCK_GET_RANDOM_PLACE = "mapcontenders.repo.place.PlaceRepo.get_random"
MOCK_GET_RANDOMS_PLACE = "mapcontenders.repo.place.PlaceRepo.get_randoms"
MOCK_GET_RANDOMS_PICTURES = "mapcontenders.repo.picture.PictureRepo.get_randoms"


def create_mock_place(seed: int = 1, **kwargs) -> PlaceModel:
    diff = kwargs.pop("difficulty", 1 + (seed % 3))
    place = PlaceModel(
        id=kwargs.pop("id", seed),
        name=kwargs.pop("name", f"name-{seed}"),
        hint=kwargs.pop("hint", None if seed % 2 == 0 else f"hint-{seed}"),
        difficulty=diff,
        geom=from_shape(Point(kwargs.pop("lon", seed - 1), kwargs.pop("lat", seed + 1)), SRID_CODE),
        creation=kwargs.pop("difficulty", datetime(year=now.year, month=now.month, day=now.day, hour=seed % 24, minute=seed % 60, second=seed % 60)),
        creator=kwargs.pop("creator", f"creator-{seed}"),
        likes=kwargs.pop("likes", 1 + seed * 3),
        dislikes=kwargs.pop("dislikes", 1 + seed * 2),
        scores_pic2point=[],
        scores_point2pic=[],
    )
    if "pictures" in kwargs:
        place.pictures = kwargs.pop("pictures", [create_mock_picture(i, place=place) for i in range(diff)])
    return place


def create_mock_picture(seed: int = 1, **kwargs) -> PictureModel:
    picture = PictureModel(
        id=kwargs.pop("id", seed),
        creation=kwargs.pop("difficulty", datetime(year=now.year, month=now.month, day=now.day, hour=seed % 24, minute=seed % 60, second=seed % 60)),
    )
    if "place_id" in kwargs:
        picture.place_id = kwargs.pop("place_id")
    if "place_id" in kwargs:
        picture.place = kwargs.pop("place")
    return picture
