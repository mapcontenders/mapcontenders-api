from flask import Response
from flask.testing import FlaskClient
from pytest_mock import MockerFixture

from tests.mocks import MOCK_GET_RANDOM_PLACE, create_mock_picture, create_mock_place


def test_get_pic2point_set(client: FlaskClient, mocker: MockerFixture):
    mocker.patch(MOCK_GET_RANDOM_PLACE, return_value=create_mock_place(10))
    r: Response = client.get("/api/guess/pic2point")
    assert r.status_code == 200

    data = r.get_json()
    assert "created" in data
    assert "difficulty" in data
    assert "pictures" in data


def test_get_pic2point_sets_difficulty(client: FlaskClient, mocker: MockerFixture):
    for diff in [1, 2, 3]:
        for seed in range(10):
            mocker.patch(MOCK_GET_RANDOM_PLACE, return_value=create_mock_place(seed, difficulty=diff, pictures=[create_mock_picture(i) for i in range(4)]))
            data = client.get("/api/guess/pic2point").get_json()
            assert len(data["pictures"]) == 4 - diff
