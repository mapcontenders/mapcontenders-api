from flask import Response
from flask.testing import FlaskClient
from jsonschema import ValidationError
from pytest import fail
from pytest_mock import MockerFixture

from mapcontenders.schema import geojson_validator
from tests.mocks import MOCK_GET_ALL_PLACES, MOCK_GET_PLACE_BY_ID, create_mock_place


def test_get_places(client: FlaskClient, mocker: MockerFixture):
    seed = 15
    mocker.patch(MOCK_GET_ALL_PLACES, return_value=[create_mock_place(seed)])
    r: Response = client.get("/api/places")
    assert r.status_code == 200

    data = r.get_json()
    assert len(data) == 1
    assert data[0]["id"] == seed
    assert data[0]["name"] == f"name-{seed}"
    assert data[0]["hint"] == f"hint-{seed}"
    assert data[0]["lon"] == seed - 1
    assert data[0]["lat"] == seed + 1
    assert data[0]["creator"] == f"creator-{seed}"
    assert data[0]["likes"] == 1 + seed * 3
    assert data[0]["dislikes"] == 1 + seed * 2


def test_get_places_geojson(client: FlaskClient, mocker: MockerFixture):
    seed1, seed2 = 17, 432
    mocker.patch(MOCK_GET_ALL_PLACES, return_value=[create_mock_place(seed1), create_mock_place(seed2)])
    r: Response = client.get("/api/places.geojson")
    assert r.status_code == 200

    data = r.get_json()
    assert data["type"] == "FeatureCollection"
    assert len(data["features"]) == 2

    assert data["features"][0]["geometry"]["type"] == "Point"
    assert data["features"][0]["geometry"]["coordinates"][0] == seed1 - 1
    assert data["features"][0]["geometry"]["coordinates"][1] == seed1 + 1
    assert data["features"][0]["properties"]["likes"] == 1 + seed1 * 3
    assert data["features"][0]["properties"]["dislikes"] == 1 + seed1 * 2

    assert data["features"][1]["geometry"]["type"] == "Point"
    assert data["features"][1]["geometry"]["coordinates"][0] == seed2 - 1
    assert data["features"][1]["geometry"]["coordinates"][1] == seed2 + 1
    assert data["features"][1]["properties"]["likes"] == 1 + seed2 * 3
    assert data["features"][1]["properties"]["dislikes"] == 1 + seed2 * 2


def test_validate_places_geojson(client: FlaskClient, mocker: MockerFixture):
    mocker.patch(MOCK_GET_ALL_PLACES, return_value=[create_mock_place(i) for i in range(10)])
    validator = geojson_validator()
    try:
        data = client.get("/api/places.geojson").get_json()
        validator.validate(data)
        for feature in data["features"]:
            validator.validate(feature)
    except ValidationError:
        fail("Invalid geojson")


def test_validate_place_geojson(client: FlaskClient, mocker: MockerFixture):
    validator = geojson_validator()
    for p in [create_mock_place(i) for i in range(10)]:
        mocker.patch(MOCK_GET_PLACE_BY_ID, return_value=p)
        try:
            validator.validate(client.get(f"/api/place/{p.id}.geojson").get_json())
        except ValidationError:
            fail("Invalid geojson")
