from flask import Response
from flask.testing import FlaskClient
from pytest_mock import MockerFixture

from tests.mocks import (
    MOCK_GET_RANDOM_PLACE,
    MOCK_GET_RANDOMS_PICTURES,
    create_mock_picture,
    create_mock_place,
)


def test_get_point2pic_set(client: FlaskClient, mocker: MockerFixture):

    mock_pictures = [create_mock_picture(i, place=create_mock_place(i)) for i in range(1, 5)]
    mock_place = create_mock_place(1, pictures=[mock_pictures[0]])
    mocker.patch(MOCK_GET_RANDOM_PLACE, return_value=mock_place)
    mocker.patch(MOCK_GET_RANDOMS_PICTURES, return_value=mock_pictures)
    r: Response = client.get("/api/guess/point2pic")
    assert r.status_code == 200

    data = r.get_json()
    assert "created" in data
    assert "difficulty" in data
    assert "place" in data
    assert "pictures" in data

    assert data["place"]["id"] == 1
    assert len(data["pictures"]) == 5
