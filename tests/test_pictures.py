import os

from flask import Response
from flask.testing import FlaskClient

from tests.data.data import create_picture_api, create_place_api, get_test_picture_path


def test_create_picture(client: FlaskClient):
    place = create_place_api(client, 1)
    id_place = place["id"]
    assert place["id"] == 1
    assert place["pictures_count"] == 0

    path = get_test_picture_path(1)
    resp: Response = client.post(f"/api/place/{id_place}/pictures", data=dict(picture_file=(open(path, "rb"), os.path.basename(path))))
    assert resp.status_code == 200
    picture = resp.get_json()
    id_picture = picture["id"]
    assert picture["id"] == 1

    place = client.get(f"/api/place/{id_place}").get_json()
    assert place["pictures_count"] == 1

    place_pictures = client.get(f"/api/place/{id_place}/pictures").get_json()
    assert len(place_pictures) == 1
    assert place_pictures[0]["id"] == id_picture

    picture = client.get(f"/api/picture/{id_picture}").get_json()
    assert picture["id"] == id_picture
    assert picture["place_id"] == id_place


def test_create_multiple_pictures(client: FlaskClient):
    place = create_place_api(client, 1)
    id_place = place["id"]

    for i in range(1, 30):
        picture = create_picture_api(client, id_place, i)
        id_picture = picture["id"]
        assert id_picture == i
        assert client.get(f"/api/place/{id_place}").get_json()["pictures_count"] == i
        assert os.path.exists(f"tests/pictures/{id_place}/{id_picture}.jpg")
