#! /bin/bash

# PostGreSQL env variables
export POSTGRES_DB="mapcontenders_test_db"
export PG_BIND_PORT=54321
export POSTGRES_USER="mapcontenders_test_user"
export POSTGRES_PASSWORD="mapcontenders_test_password"

# Flask env variables
export API_VERSION="test-version"

# SQLAlchemy env variables
export DB_HOST="localhost"
export DB_PORT=$PG_BIND_PORT
export DB_NAME=$POSTGRES_DB
export DB_USER=$POSTGRES_USER
export DB_PASSWORD=$POSTGRES_PASSWORD
