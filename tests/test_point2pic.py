from flask import Response
from flask.testing import FlaskClient

from tests.data.data import create_dataset


def test_get_point2pic_set(client: FlaskClient):
    create_dataset(client, 2, lambda p: 2)
    r: Response = client.get("/api/guess/point2pic")
    assert r.status_code == 200


def test_multiple_pic2point_with_diff_correct_pictures(client: FlaskClient):
    create_dataset(client, 20, lambda p: 5)

    for i in range(50):
        difficulty = (i % 3) + 1
        data = client.get(f"/api/guess/point2pic?difficulty={difficulty}").get_json()
        assert data["difficulty"] == difficulty
        place = data["place"]
        assert place["difficulty"] == difficulty

        # check the correct number of provided pictures (depends on difficulty)
        pictures = data["pictures"]
        assert len(pictures) == difficulty + 1

        # check that only one picture is the correct one
        place_pictures = client.get(f"/api/place/{place['id']}/pictures").get_json()
        place_picture_ids = [p["id"] for p in place_pictures]
        assert len([p for p in pictures if p["id"] in place_picture_ids]) == 1
        assert len([p for p in pictures if p["id"] not in place_picture_ids]) == difficulty


def test_multiple_pic2point_no_diff_correct_pictures(client: FlaskClient):
    create_dataset(client, 20, lambda p: 5)

    for _ in range(20):
        data = client.get(f"/api/guess/point2pic").get_json()
        assert data["difficulty"] is None
        place = data["place"]

        # check the correct number of provided pictures (depends on difficulty)
        pictures = data["pictures"]
        assert len(pictures) == place["difficulty"] + 1

        # check that only one picture is the correct one
        place_pictures = client.get(f"/api/place/{place['id']}/pictures").get_json()
        place_picture_ids = [p["id"] for p in place_pictures]
        assert len([p for p in pictures if p["id"] in place_picture_ids]) == 1
        assert len([p for p in pictures if p["id"] not in place_picture_ids]) == place["difficulty"]
