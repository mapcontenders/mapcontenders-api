import pytest
from flask import Flask

import settings
from mapcontenders.app import create_app
from mapcontenders.database import db


@pytest.fixture(scope="session")
def app():
    setattr(settings, "FLASK_ENV", "test")
    setattr(settings, "FLASK_DEBUG", True)
    setattr(settings, "TESTING", True)
    setattr(settings, "API_TITLE", "Test API")
    setattr(settings, "PICTURES_FOLDER", "tests/pictures")
    app = create_app(settings)
    yield app


@pytest.fixture()
def context(app: Flask):
    with app.test_request_context():
        yield app


@pytest.fixture()
def client(app):
    yield app.test_client()


@pytest.fixture(autouse=True)
def run_around_tests(app):
    """
    executed around every unit test
    """
    # create schema
    db.create_all()
    yield
    # drop all
    db.session.remove()
    db.drop_all()
