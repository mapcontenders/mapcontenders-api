# Mapcontenders tests

Here are some `curl` commands to test API

## pic2point

- get a pic2point guess set:

```bash
curl "http://localhost:5000/api/guess/pic2point"
```

- try a pic2point guess and get result:

```bash
curl  -H "Content-Type: application/json" \
  -d '{"created": "2021-01-01T12:00:00", "id_pictures": [1,2], "guessed_lon":2.29, "guessed_lat": 48.86}' \
  -X POST "http://localhost:5000/api/guess/pic2point"
```

## point2pic

- get a point2pic guess set:

```bash
curl "http://localhost:5000/api/guess/point2pic"
```

- try a point2pic guess and get result:

```bash
curl  -H "Content-Type: application/json" \
  -d '{"created": "2021-01-01T12:00:00", "id_place": 1, "id_pictures": [1,3], "guessed_id_picture": 1}' \
  -X POST "http://localhost:5000/api/guess/point2pic"
```
