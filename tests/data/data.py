import os
from typing import Any, Callable, Dict, List

from flask import Response
from flask.testing import FlaskClient

from mapcontenders.model.place import MAX_DIFF

# region Places


def create_place_body(seed: int = 1, **kwargs) -> Dict[str, Any]:
    return dict(
        name=kwargs.pop("name", f"test-name-{seed}"),
        hint=kwargs.pop("hint", f"test-hint-{seed}"),
        difficulty=kwargs.pop("difficulty", (seed % MAX_DIFF) + 1),
        lon=kwargs.pop("lon", seed),
        lat=kwargs.pop("lat", seed),
        creator=kwargs.pop("creator", f"test-creator-{seed}"),
    )


def create_place_api(client: FlaskClient, seed: int = 1, **kwargs) -> Dict[str, Any]:
    resp: Response = client.post("/api/place", json=create_place_body(seed, **kwargs))
    assert resp.status_code == 200
    return resp.get_json()


def create_multiple_places(client: FlaskClient, rng: range) -> List[Dict[str, Any]]:
    return [create_place_api(client, i) for i in rng]


# endregion


# region Pictures


def get_test_picture_path(num: int) -> str:
    return f"tests/data/pictures/{(num % 10) + 1}.jpg"


def create_picture_api_with_path(client: FlaskClient, id_place: int, path: str) -> Dict[str, Any]:
    resp: Response = client.post(f"/api/place/{id_place}/pictures", data=dict(picture_file=(open(path, "rb"), os.path.basename(path))))
    assert resp.status_code == 200
    return resp.get_json()


def create_picture_api(client: FlaskClient, id_place: int, num_pic: int) -> Dict[str, Any]:
    return create_picture_api_with_path(client, id_place, get_test_picture_path(num_pic))


# endregion


# region Datasets


def create_dataset(client: FlaskClient, nb_places: int, nb_pictures_by_place: Callable[[Dict[str, Any]], int]) -> List[Dict[str, Any]]:
    places = create_multiple_places(client, range(1, nb_places + 1))
    for place in places:
        for i in range(nb_pictures_by_place(place)):
            create_picture_api(client, place["id"], i)
    return client.get("/api/places").get_json()


# endregion
