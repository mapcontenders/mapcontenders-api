import os

# Flask
FLASK_ENV = os.environ.get("FLASK_ENV", "production")
FLASK_DEBUG = os.environ.get("FLASK_DEBUG", False)
FLASK_USE_RELOAD = FLASK_DEBUG
SHOW_STACKTRACE = FLASK_DEBUG
WTF_CSRF_ENABLED = True

# PostGreSQL database connection
DB_HOST = os.environ.get("DB_HOST", os.environ.get("PG_HOST"))
DB_PORT = os.environ.get("DB_PORT", os.environ.get("PG_BIND_PORT"))
DB_NAME = os.environ.get("DB_NAME", os.environ.get("POSTGRES_DB"))
DB_USER = os.environ.get("DB_USER", os.environ.get("POSTGRES_USER"))
DB_PASSWORD = os.environ.get("DB_PASSWORD", os.environ.get("POSTGRES_PASSWORD"))
# SQLAlchemy database connection
SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
SQLALCHEMY_ENGINE_OPTIONS = {"pool_size": os.environ.get("DB_POOL_SIZE", 3)}
SQLALCHEMY_ECHO = os.environ.get("SQLALCHEMY_ECHO", False)
SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("SQLALCHEMY_TRACK_MODIFICATIONS", False)
SQLALCHEMY_RECORD_QUERIES = os.environ.get("SQLALCHEMY_RECORD_QUERIES", False)

# API
API_TITLE = "MapContenders API"
API_VERSION = os.environ.get("API_VERSION")
JSON_AS_ASCII = False
ERROR_404_HELP = False
ENABLE_PROXY_FIX = True  # Extract and use X-Forwarded-For/X-Forwarded-Proto headers?
PAGINATE_DEFAULT_LIMIT = 50

# IMAGES
ALLOWED_PICTURES_MIMETYPES = ["image/jpeg", "image/png"]
MAX_PICTURE_DIMENSION = 1080
JPEG_QUALITY = 90
PICTURES_FOLDER = "pictures"

# MAPBOX
MAPBOX_BASE_STYLE = os.environ.get("MAPBOX_BASE_STYLE", "https://guilhemstreetmap.guilhemallaman.net/styles/klokantech-basic/style.json")

# OPENAPI
OPENAPI_VERSION = "3.0.3"
OPENAPI_URL_PREFIX = "/openapi"
# SWAGGER UI
OPENAPI_SWAGGER_UI_PATH = "/swagger"
OPENAPI_SWAGGER_UI_URL = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"
# REDOC UI
OPENAPI_REDOC_PATH = "/redoc"
OPENAPI_REDOC_URL = "https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"

# CACHING
CACHE_TYPE = "RedisCache"
CACHE_DEFAULT_TIMEOUT = 500
CACHE_REDIS_HOST = os.environ.get("CACHE_REDIS_HOST", "localhost")
CACHE_REDIS_PORT = os.environ.get("CACHE_REDIS_PORT", 6379)
CACHE_REDIS_DB = 0
CACHE_KEY_PREFIX = "mapcontenders."
