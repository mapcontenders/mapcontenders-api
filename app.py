from logging.config import fileConfig

# import models module here to include those models in migrations (via flask-migrate)
import mapcontenders.model  # noqa
from mapcontenders.app import create_app

fileConfig("logging.conf", disable_existing_loggers=True)

application = create_app()
