# MapContenders API


API written in python using Flask

OpenApi doc can be found here:

- [Swagger](https://api.mapcontenders.guilhemallaman.net/openapi/swagger)
- [Redoc](https://api.mapcontenders.guilhemallaman.net/openapi/redoc)


**BEFORE RUNNING ANY FLASK COMMAND, REQUIRED ENVIRONMENT VARIABLES SET MUST BE SET.
EVERY FLASK COMMAND MUST BE RUN INSIDE POETRY CONTEXT WITH `poetry run flask ...`**



## Setup

- Install dependencies:
```bash
poetry install
```

- Install pre-commit hooks:
```bash
poetry run pre-commit install
```

- Run code-formatting tools :
```bash
poetry run pre-commit run --all-files
poetry run black [-l 128] .
poetry run isort .
poetry run mypy . --ignore-missing-import
```

- Upgrade pip dependencies :
```bash
# see
poetry-up --dry-run # checks which deps to update
poetry-up --no-commit
poetry install
```



## Serve

Flask app needs following env variables:

- optional env

```bash
# Flask env settings
export FLASK_ENV=production                 # default: "development"
export FLASK_DEBUG=True                     # default: False
# SQLAlchemy database connection
export SQLALCHEMY_ECHO=True                 # default: False (check log levels)
export DB_POOL_SIZE=2                       # default: 3
export SQLALCHEMY_TRACK_MODIFICATIONS=True  # default: False
export SQLALCHEMY_RECORD_QUERIES=True       # default: False
# cache
export CACHE_REDIS_HOST=my.domain           # default: "localhost"
export CACHE_REDIS_PORT=12345               # default: 6379
```

- required env variables

```bash
# API version (should be fetched from poetry's pyproject.toml)
export API_VERSION=$(poetry version -s)
# PostGreSQL connection settings (required by flask)
export DB_HOST=my_db_host       # database host
export DB_PORT=12345            # database port
export DB_NAME=my_database      # database name
export DB_USER=my_user          # database user
export DB_PASSWORD=my_password  # database password for user
# Docker
export APP_BIND_PORT=12345      # port on host binded by Docker
# pictures folder on docker host
export PICTURES_FOLDER=/path/to/pictures/folder
```

Serve commands:

```bash
# using uwsgi server
# gives more control about meta: workers, logging, sockets ...
poetry run uswgi <UWSGI-CONFIG>.ini

# standalone flask
export FLASK_APP=app.py
poetry run flask run
```



## Build

Build docker image with `docker build -t mapcontenders/api:latest .`

Build and run docker-compose setup with `docker-compose up --build`.
This will run flask server with uwsgi exposed on port set by `$APP_BIND_PORT` env variable
On docker host machine, the static volume path containing pictures must be set with var `$PICTURES_FOLDER`


## Database

### Setup

The mapcontenders db is in a docker container and orchestered with docker-compose:

```bash
# run only database
docker-compose up --detach db
# run database and web pgadmin
docker-compose up --detach
```

To setup database, you first need to set following environment variables: (required by Docker image)

    - POSTGRES_DB=my_db
    - PG_BIND_PORT=5432
    - POSTGRES_USER=my_user
    - POSTGRES_PASSWORD=my_password

PgAdmin needs those environement variables:

    - PGADMIN_DEFAULT_EMAIL=my_email@my.domain
    - PGADMIN_DEFAULT_PASSWORD=my_password
    - PGADMIN_LISTEN_PORT=5433

### Migrate

Database migrations are handled with `flask-migrate` tool

Environment variables must be set before so that Flask app can connect to database

- init migration (do this only once at the beginning of the python project):
```bash
poetry run flask db init
```

- compute migration (database must be up-to-date):
```bash
poetry run flask db migrate
```
This will create a new revision file in `migrations/versions`

- apply migration (after checking migrate revision file):
```bash
poetry run flask db upgrade
```

### Initialize data

Basic data can be populated with following Flask cli command (required env vars must be set):

```bash
poetry run flask populate_db
```

### Cheat sheet

```bash
docker-compose down
docker system prune
docker volume prune
docker-compose up --detach db
poetry run flask upgrade
poetry run flask populate_db
```
