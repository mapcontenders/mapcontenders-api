variables:
  GIT_DEPTH: 0
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"


before_script:
  - export API_VERSION="$(poetry version -s)"


stages:
  - setup
  - test
  - analyse
  - migrate
  - deploy


########################
######   SETUP   ######
########################
setup:poetry:
  stage: setup
  tags:
    - shell
  only:
    changes:
      - .gitlab-ci.yml
      - pyproject.toml
      - poetry.lock
  script:
    - poetry install


########################
######   TEST    #######
########################
test:poetry:
  stage: test
  tags:
    - shell
  before_script:
    - echo "*** setting test env variables ***"
    - . tests/env-test.sh
    - echo "*** popping up database ***"
    - docker pull kartoza/postgis:13-3.1
    - docker-compose up --remove-orphans --detach db-test
  script:
    - poetry run black -l 156 . --check
    - poetry run isort . --check
    - poetry run mypy . --check --ignore-missing-imports --exclude migrations
    - sleep 0.5
    - echo "1 crocodile ..."
    - sleep 0.5
    - echo "2 crocodiles ..."
    - sleep 0.5
    - echo "3 crocodiles ..."
    - sleep 0.5
    - poetry run pytest --cov --cov-report xml:.coverage.xml
  after_script:
    - echo "*** popping down database ***"
    - docker-compose stop db-test
    - echo "*** clearing test pictures ***"
    - rm -rf tests/pictures
  artifacts:
    paths:
      - .coverage.xml
    expire_in: 1 hour


########################
#####   ANALYSE   ######
########################
analyse:poetry:
  stage: analyse
  tags:
    - shell
  script:
    - sh analyse.sh
  allow_failure: true


########################
######   MIGRATE   #####
########################
# migrate database is manually triggered (on master branch only)
# before trigger this migration, make sure database is up: (after setting env vars)
#   docker-compose up --detach db
migrate:
  stage: migrate
  tags:
    - prod
  only:
    refs:
      - master
    changes:
      - db/**/*
      - migrations/**/*
  when: manual
  interruptible: true
  script:
    - export DB_HOST=$PG_HOST
    - export DB_PORT=$PG_BIND_PORT
    - export DB_NAME=$POSTGRES_DB
    - export DB_USER=$POSTGRES_USER
    - export DB_PASSWORD=$POSTGRES_PASSWORD
    - poetry run flask db upgrade
    - poetry run flask populate_db


########################
######   DEPLOY   ######
########################
# deploy is manually triggered (on master branch only)
# before triggering deploy, make sure database is ok
deploy:
  stage: deploy
  tags:
    - prod
  only:
    - master
  when: manual
  interruptible: true
  environment:
    name: prod/mapcontenders
    url: https://api.mapcontenders.guilhemallaman.net/openapi/swagger
  script:
    - docker-compose up --build --detach redis flask
